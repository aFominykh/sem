import java.util.LinkedList;
import java.util.List;

public class SimpleBuffer<T> implements IBuffer<T> {
    private List<T> queue;
    SimpleBuffer(T... n){
        queue = new LinkedList<T>();
        for(int i = 0; i < n.length; i++){
            queue.add(n[i]);
        }
    }

    public void addQueueElem(T elem){
        queue.add(elem);
    }

    public T getQueueElem(){
        return queue.remove(0);
    }

    public int getSize(){
        return queue.size();
    }

    public void del(){
        queue.clear();
    }

    public boolean isEmpty(){
        return queue.isEmpty();
    }
}
