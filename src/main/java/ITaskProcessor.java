public interface ITaskProcessor {
    Integer process(Task task);
}
