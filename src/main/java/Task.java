import java.util.Arrays;

public class Task {
    private int[] arr;
    Task(int... arr){
        this.arr = new int[arr.length];
        for(int i = 0; i < arr.length; i++){
            this.arr[i] = arr[i];
        }
    }

    public int getArr(int i) {
        return arr[i];
    }

    public int[] getData(){
        return arr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(arr, task.arr);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }
}
