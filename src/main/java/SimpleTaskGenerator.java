public class SimpleTaskGenerator implements ITaskGenerator{
    private SimpleBuffer buffer;
    private int startValue, amount;

    SimpleTaskGenerator(SimpleBuffer buffer, int startValue, int amount){
        this.startValue = startValue;
        this.amount = amount;
        this.buffer = buffer;
    }

    public void generate() {
        int help = startValue;
        int[] arr = new int[amount];
        for(int i = 0; i < amount; i++){
            arr[i] = help;
            help++;
        }
        Task task = new Task(arr);
        buffer.addQueueElem(task);
    }

    public SimpleTaskGenerator withStartValue(int startValue){
        this.startValue = startValue;
        return this;
    }

    public SimpleTaskGenerator withAmount(int amount){
        this.amount = amount;
        return this;
    }
}
