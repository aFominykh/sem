public class SimpleTaskProcessor implements ITaskProcessor {
    private SimpleBuffer buffer;

    SimpleTaskProcessor(SimpleBuffer buffer){
        this.buffer = buffer;
    }


    public Integer process(Task task/*<- Я не понимаю зачем это*/) {
        if (buffer.isEmpty()){
            return null;
        }
        int sum = 0;
        Task task1 = (Task) buffer.getQueueElem();
        for (int i = 0; i < task1.getData().length; i++){
            sum = sum + task1.getArr(i);
        }
        return sum;
    }
}
