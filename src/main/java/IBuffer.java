public interface IBuffer<T> {
    void addQueueElem(T i);
    T getQueueElem();
    int getSize();
    void del();
    boolean isEmpty();
}
