import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleTaskGeneratorTest {
    Task task = new Task(1, 2, 3, 4, 5);
    SimpleBuffer buffer = new SimpleBuffer();
    SimpleTaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 5);

    @Test
    void generate() {
        generator.generate();
        Task task1 = (Task) buffer.getQueueElem();
        for (int i = 0; i < buffer.getSize(); i++) {
            assertEquals(task.getArr(i), task1.getArr(i));
        }
    }

    @Test
    void withStartValue() {
        generator.withStartValue(1).withAmount(5).generate();
        Task task1 = (Task) buffer.getQueueElem();
        for (int i = 0; i < buffer.getSize(); i++) {
            assertEquals(task.getArr(i), task1.getArr(i));
        }
    }

    @Test
    void withAmount() {
        generator.withStartValue(1).withAmount(5).generate();
        Task task1 = (Task) buffer.getQueueElem();
        for (int i = 0; i < buffer.getSize(); i++) {
            assertEquals(task.getArr(i), task1.getArr(i));
        }
    }
}