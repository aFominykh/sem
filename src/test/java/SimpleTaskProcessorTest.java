import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleTaskProcessorTest {
    Task task = new Task(1, 2, 3, 4);
    Task task1 = new Task(1, 2, 3, 4, 6, 2);
    SimpleBuffer buffer = new SimpleBuffer(task);
    SimpleBuffer buffer1 = new SimpleBuffer(task1);
    SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);
    SimpleTaskProcessor processor1 = new SimpleTaskProcessor(buffer1);

    @Test
    void process() {
        assertEquals(10, processor.process(task));
        assertEquals(18, processor1.process(task));
        assertEquals(null, processor1.process(task));
    }
}