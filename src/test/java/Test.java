import static org.junit.jupiter.api.Assertions.*;

class Test {

    SimpleBuffer buffer = new SimpleBuffer(1,2,3,4,5);
    SimpleBuffer buffer1 = new SimpleBuffer(1,2,3,4,5,6);

    @org.junit.jupiter.api.Test
    void addQueueElem() {
        buffer.addQueueElem(6);
        for (int i = 0; i < buffer.getSize(); i++) {
            assertEquals(buffer1.getQueueElem(),buffer.getQueueElem());
        }
    }

    @org.junit.jupiter.api.Test
    void getQueueElem() {
            assertEquals(1,buffer.getQueueElem());
    }

    @org.junit.jupiter.api.Test
    void getSize() {
        assertEquals(5, buffer.getSize());
    }

    @org.junit.jupiter.api.Test
    void del() {
        assertEquals(false, buffer.isEmpty());
        buffer.del();
        assertEquals(true, buffer.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
        assertEquals(false, buffer.isEmpty());
        buffer.del();
        assertEquals(true, buffer.isEmpty());
    }
}